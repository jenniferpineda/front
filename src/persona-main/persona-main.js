import { LitElement, html } from 'lit-element'; 
import '../persona-ficha-listado/persona-ficha-listado.js'; 
import '../persona-form/persona-form.js';  

class PersonaMain extends LitElement {
	static get properties() {
		return {			
			people: {type: Array},
			showPersonForm: {type: Boolean}
		};
	}			

	constructor() {
		super();
			
		this.people = [
			{
				name: "Ellen Ripley",
				yearsInCompany: 10,
				profile: "Personaje principal de la saga de ciencia ficción Alien, protagonizado por la actriz estadounidense Sigourney Weaver. ",
				photo: {
					"src": "./img/persona.jpg",
					"alt": "Ellen Ripley"
				},
				canTeach: false				
			}, {
				name: "Bruce Banner",		
				yearsInCompany: 2,
				profile: "El Doctor Robert Bruce Banner es un científico de renombre y miembro fundador de los Vengadores.",
				photo: {
					"src": "./img/persona.jpg",
					"alt": "Bruce Banner"
				},
				canTeach: true
			}, {
				name: "Éowyn",
				yearsInCompany: 5,
				profile: "Personaje de la novela fantástica El Señor de los Anillos, de J. R. R. Tolkien.",
				photo: {
					"src": "./img/persona.jpg",
					"alt": "Éowyn"
				},
				canTeach: true
			}, {
				name: "Turanga Leela",
				yearsInCompany: 9,
				profile: "Personaje de la serie animada Futurama, mutante de alcantarilla, capitana de Planet Express.",
				photo: {
					"src": "./img/persona.jpg",
					"alt": "Turanga Leela"
				},
				canTeach: true
			}, {
				name: "Tyrion Lannister",
				yearsInCompany: 1,
				profile: "Personaje ficticio de la saga Canción de hielo y fuego de George R. R. Martin y de su correspondiente adaptación televisiva.",
				photo: {
					"src": "./img/persona.jpg",
					"alt": "Tyrion Lannister"
				},
				canTeach: false
			}
		];
        
	this.showPersonForm = false;
    }
		
    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="row" id="peopleList" style="margin-top:60px; margin-bottom:40px">			
                <div class="row row-cols-1 row-cols-md-4 mb-3" style="margin-right:20px">
                    ${this.people.map(
                        person => 
                        html`<persona-ficha-listado 
                                name="${person.name}" 
                                yearsInCompany="${person.yearsInCompany}" 
                                profile="${person.profile}" 
                                .photo="${person.photo}"
                                @delete-person="${this.deletePerson}">
                            </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}" >
                </persona-form>
            </div>
        `;
    }

    updated(changedProperties) {
        console.log("updated");	
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
    
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	  
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	 	  
    }

    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
    
        this.showPersonForm = false;	
    }

    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");	
                        
        this.people.push(e.detail.person);
    
        console.log("Persona almacenada");	
        this.showPersonForm = false;
    }
}
customElements.define('persona-main', PersonaMain)