import { LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
                        <nav class="navbar navbar-inverse fixed-top" style="background-color: #D1F2EB">
                          <div class="container-fluid">
                            <div class="navbar-header">
                              <a class="navbar-brand" href="">App Persona</a>	
                            </div>
                        </div>
                        </nav>
        `;
    }
}

customElements.define('persona-header', PersonaHeader)